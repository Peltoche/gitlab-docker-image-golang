

FROM	debian:8.3
MAINTAINER	Peltoche
RUN		apt-get update && apt-get install -y \
			golang \
			git \
			make \
			curl \
			libsnappy-dev \
			autoconf \
			automake \
			libtool \
			pkg-config

RUN		git clone https://github.com/openvenues/libpostal

WORKDIR	libpostal

RUN		./bootstrap.sh && \
			./configure --datadir=$(pwd)/data && \
			make && \
			make install

WORKDIR	..

RUn		mkdir $(pwd)/deps $HOME/go

ENV		GOPATH=$HOME/go
ENV		PKG_CONFIG_PATH=$(pwd)/deps/lib/pkgconfig:$PKG_CONFIG_PATH
ENV		LD_LIBRARY_PATH=$(pwd)/deps/lib:$LD_LIBRARY_PATH
